from flask import Flask
import os

app = Flask(__name__)

@app.route('/d')
def runner():
    message = "{name} is running!"
    return message.format(os.getenv("NAME", "FLASK"))


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)