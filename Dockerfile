FROM python:3.8.0-slim

WORKDIR /app

ADD . /app
RUN pip install --trusted-host pypi.python.org Flask

ENV NAME Mark

# For Django
# EXPOSE 8000
# CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

CMD ["python", "app.py"]
